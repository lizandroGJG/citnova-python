from datetime import datetime
from Persona import Persona

class Conductor(Persona):
    licencia    = None
    entrada     = None
    salida      = None

    def __init__(self, licencia=None, entrada=None, salida=None, persona=None):
        if persona is not None and isinstance(persona, Persona):
            super(Conductor, self).__init__(nombre=persona.nombre, apellidos=persona.apellidos, genero=persona.genero, fecnac=persona.fecnac)

        self.licencia   = licencia
        self.entrada    = entrada
        self.salida     = salida

    def __str__(self):
        return '{}'.format(self.licencia)


    def registrar_entrada(self, hora=None):
        self.entrada    = hora

    
    def registrar_salida(self, hora=None):
        self.salida     = hora

    
    def conducir(*args, **kwargs):
        return 'Estoy conduciendo'


def main():
    lizandro = Persona('Lizandro Gabriel', 'Jimenez Garcia', 'H', datetime(1995, 7, 3))
    conductor = Conductor(licencia='licencia', persona=lizandro)
    print("nombre: {} \nedad: {} años \nentrada: {}".format(conductor.nombre, conductor.getEdad(), conductor.entrada))


if __name__ == "__main__":
    main()
    
    
