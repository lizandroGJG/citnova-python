# from datetime import date
from datetime import datetime
from Persona import Persona

class Pasajero(Persona):
    boleto = None
    forma_pago = None
    abordo = None

    def __init__(self, boleto=None, forma_pago=None, abordo=None, persona=None):
        if persona is not None and isinstance(persona, Persona):
            super(Pasajero, self).__init__(nombre=persona.nombre, apellidos=persona.apellidos, genero=persona.genero, fecnac=persona.fecnac)
        
        self.boleto = boleto
        self.forma_pago = forma_pago
        self.abordo = abordo

    def compra_boleto(self, forma_pago):
        self.forma_pago = forma_pago

    def subir_vehiculo(self):
        self.abordo = datetime.now()


def main():
    pepito = Persona('Pepito', 'Jimenez Garcia', 'H', datetime(1995, 7, 3))
    pasajero = Pasajero(boleto='25H', persona=pepito)
    print("nombre: {} \nedad: {} años \nboleto: {}".format(pasajero.nombre, pasajero.get_edad(), pasajero.boleto))
    print("edad del pasajero: {}".format(pasajero.get_edad()))

    pasajero2 = Pasajero("34D")
    pasajero2.nombre = "Jaimito"
    print("{} boleto: {}".format(pasajero2.get_nombre_completo(), pasajero2.boleto))


if __name__ == "__main__":
    main()
