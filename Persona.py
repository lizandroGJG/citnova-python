# from datetime import date
from datetime import datetime


class Persona:
    nombre = None
    apellidos = None
    genero = None
    fecnac = None

    def __init__(self, nombre=None, apellidos=None, genero=None, fecnac=None):
        self.nombre = nombre
        self.apellidos = apellidos
        self.genero = genero
        self.fecnac = fecnac

    def get_nombre_completo(self):
        return "{} {}".format(self.nombre, self.apellidos)

    def get_edad(self):
        if self.fecnac is not None:
            hoy = datetime.now()
            edad = hoy.year - self.fecnac.year
            return edad
        return "Sin fecha de nacimiento"

    def __str__(self):
        return self.get_nombre_completo()

    def __le__(self, other):
        return self.nombre.upper() <= other.nombre.upper()

    def __lt__(self, other):
        return self.nombre.upper() < other.nombre.upper()

    def __ge__(self, other):
        return self.nombre.upper() >= other.nombre.upper()

    def __gt__(self, other):
        return self.nombre.upper() > other.nombre.upper()

    def __eq__(self, other):
        return self.nombre.upper() == other.nombre.upper()

    def __ne__(self, other):
        return self.nombre.upper() != other.nombre.upper()


def main():
    objeto_1 = Persona("Juan")
    objeto_2 = Persona("Carlos")

    ivan_nac = datetime(1986, 5, 24)
    objeto_3 = Persona(nombre="Ivan", fecnac=ivan_nac)

    lista = [objeto_1, objeto_2, objeto_3]

    print("Lista desordenada")
    for o in lista:
        print(o)

    print("Lista ordenada")
    lista.sort()
    for o in lista:
        print(o)

    edad_ivan = objeto_3.get_edad()
    print("Edad de Ivan: {} años".format(edad_ivan))
    objeto_3.apellidos = "Santos"
    print(objeto_3.get_nombre_completo())


if __name__ == "__main__":
    main()
