from django.contrib import admin

from .models import Viaje

class ViajeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Viaje, ViajeAdmin)