from django.db import models
import pytz

class Viaje(models.Model):
    origen = models.CharField(max_length=50, null=False, blank=False)
    destino = models.CharField(max_length=50, null=False, blank=False)
    salida = models.DateTimeField(null=False, blank=False)
    llegada = models.DateTimeField(null=False, blank=False)
    costo = models.DecimalField(max_digits=6, decimal_places=2, null=False, blank=False)

    class Meta:
        verbose_name = 'Viajes'
        verbose_name_plural = 'Viajes'

    def __str__(self):
        return '{}->{} ({})'.format(self.origen, self.destino, self.salida.replace(tzinfo=None))
    