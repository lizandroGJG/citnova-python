from django.shortcuts import get_object_or_404, redirect
from django.http import JsonResponse
from django.http import Http404
from django.http import HttpResponse
from django.template import loader
from django.urls import reverse
import json

from .models import Persona, Conductor, Pasajero
from .forms import PersonaForm, ConductorForm, PasajeroForm

def listado_personas(request):
    personas = Persona.objects.all()
    context = {
        'personas': personas,
    }

    template = loader.get_template('usuarios/listado_persona.html')
    return HttpResponse(template.render(context, request))


def detalle_persona(request, persona_id):
    if persona_id is not None and isinstance(persona_id, int):
        persona = get_object_or_404(Persona, pk=persona_id)
        if request.method == 'POST':
            form = PersonaForm(request.POST, instance=persona)
            if form.is_valid():
                form.save()
                return redirect('/usuarios/personas')
        else:
            form = PersonaForm(instance=persona)

        
        context = {
            'form': form,
            'pk': persona_id,
        }

        template = loader.get_template('usuarios/detalle_persona.html')
        return HttpResponse(template.render(context, request))

    return Http404('No existe la persona seleccionada')


def registrar_persona(request):
    form = PersonaForm()
    if request.method == 'POST':
        form = PersonaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/usuarios/personas')

    context = {
        'form': form,
    }   

    template = loader.get_template('usuarios/persona_agregar.html')
    return HttpResponse(template.render(context, request))

def eliminar_persona(request, persona_id):
    if persona_id is not None and isinstance(persona_id, int):
        persona = get_object_or_404(Persona, pk=persona_id)
        persona.delete()
        return redirect('/usuarios/personas')

    return Http404('No existe la persona seleccionada')