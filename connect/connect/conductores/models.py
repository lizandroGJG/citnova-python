from django.db import models
from connect.viajes.models import Viaje
from datetime import date, datetime, time
import pytz

GENERO_CHOICES = [
    ('M', 'MASCULINO'),
    ('F', 'FEMENINO')
]

FORMAS_PAGO = [
    (1, 'TARJETA'),
    (2, 'EFECTIVO')
]

class Persona(models.Model):
    nombre    = models.CharField(max_length=50, null=False, blank=False)
    apellidos = models.CharField(max_length=50, null=False, blank=False)
    genero    = models.CharField(max_length=1, null=False, blank=False, choices=GENERO_CHOICES)
    fecnac    = models.DateField(null=False, blank=False)
    padre     = models.ForeignKey('Persona', null=True, blank=True, related_name='hijos_del_padre', on_delete=models.CASCADE)
    madre     = models.ForeignKey('Persona', null=True, blank=True, related_name='hijos_de_la_madre', on_delete=models.CASCADE)
    hermanos  = models.ManyToManyField('Persona', blank=True, related_name='mis_hermanos', through='Hermandad')
    pareja    = models.OneToOneField('Persona', null=True, blank=True, related_name='mi_pareja', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Personas'
        verbose_name_plural = 'Personas'

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellidos)

    @property
    def tiene_padre(self):
        return self.padre is not None

    @property
    def tiene_madre(self):
        return self.madre is not None

    @property
    def tiene_pareja(self):
        return self.pareja is not None

    @property
    def tiene_hermanos(self):
        return self.hermanos.all().count() > 0

    @property
    def nombre_completo(self):
        return '{} {}'.format(self.nombre, self.apellidos)

class Conductor(Persona):
    licencia = models.CharField(max_length=50, null=False, blank=False)
    entrada  = models.TimeField(null=False, blank=False) 
    salida   = models.TimeField(null=False, blank=False)

    class Meta:
        verbose_name = 'Conductores'
        verbose_name_plural = 'Conductores'

    def __str__(self):
        return '{}'.format(self.licencia)

    @property
    def ultimo_viaje(self):
        return ''

    @property
    def estatus(self):
        return ''

class Pasajero(Persona):
    boleto     = models.CharField(max_length=50, null=False, blank=False)
    forma_pago = models.IntegerField(null=False, blank=False, choices=FORMAS_PAGO, default=2)
    viaje      = models.ForeignKey(Viaje, related_name="viaje", on_delete=models.CASCADE, null=False, blank=False)

    class Meta:
        verbose_name = 'Pasajeros'
        verbose_name_plural = 'Pasajeros'

    def __str__(self):
        return '{} - {}'.format(self.viaje, self.boleto)

    @property
    def viaje_origen(self):
        return '{}'.format(self.viaje.origen)

    @property
    def viaje_destino(self):
        return '{}'.format(self.viaje.destino)

    @property
    def viaje_salida(self):
        return '{}'.format(self.viaje.salida.replace(tzinfo=None))

    @property
    def viaje_llegada(self):
        return '{}'.format(self.viaje.llegada.replace(tzinfo=None))

    @property
    def viaje_status(self):
        start   = self.viaje.salida
        end     = self.viaje.llegada
        today   = datetime.now(pytz.timezone('America/Mexico_City')).replace(tzinfo=start.tzinfo)
        status  = 'PENDIENTE' if start>today else 'FINALIZADA' if end<=today else 'EN CURSO'
        # status  = pytz.UTC.localize(today.replace(tzinfo=None))
        # status  = today
        return '{}'.format(status)


class Hermandad(models.Model):
    persona_uno = models.ForeignKey(Persona, related_name='ref_from_set', on_delete=models.CASCADE)
    persona_dos = models.ForeignKey(Persona, related_name='ref_to_set', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Hermandad'
        verbose_name_plural = 'Hermandad'

    def __str__(self):
        persona_uno = getattr(self, 'persona_uno', None)
        persona_dos = getattr(self, 'persona_dos', None)

        return "{} es hermano de {}".format(persona_uno, persona_dos) if persona_uno is not None and persona_dos is not None else ""