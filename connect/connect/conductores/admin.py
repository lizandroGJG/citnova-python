from django.contrib import admin
from .models import Conductor, Pasajero, Persona, Hermandad
from .forms import PersonaForm, HermandadForm, HermandadFormSet, ConductorForm, PasajeroForm


class PersonaTabularAdmin(admin.TabularInline):
    model = Persona
    extra = 0
    max_num = 1
    form = PersonaForm


class PadreTabularAdmin(PersonaTabularAdmin):
    fk_name = 'padre'
    verbose_name = 'Padre'
    verbose_name_plural = 'Padre'


class MadreTabularAdmin(PersonaTabularAdmin):
    fk_name = 'madre'
    verbose_name = 'Madre'
    verbose_name_plural = 'Madre'


class ParejaTabularAdmin(PersonaTabularAdmin):
    fk_name = 'pareja'
    verbose_name = 'Pareja'
    verbose_name_plural = 'Pareja'


class HermanoTabularAdmin(admin.TabularInline):
    model = Hermandad
    extra = 0
    form = HermandadForm
    formset = HermandadFormSet
    fk_name = 'persona_uno'
    verbose_name = 'Hermano'
    verbose_name_plural = 'Hermano'

class PasajeroTabularAdmin(admin.TabularInline):
    model = Pasajero
    extra = 0
    max_num = 1
    form = PasajeroForm


# class PadreTabularAdmin(PersonaTabularAdmin):
#     fk_name = 'padre'
#     verbose_name = 'Padre'
#     verbose_name_plural = 'Padre'


class PersonaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellidos', 'genero', 'tiene_padre', 'tiene_madre', 'tiene_hermanos', 'tiene_pareja']

    search_fields = ['nombre', 'apellidos']

    list_filter = ['apellidos', 'genero']

    form = PersonaForm

    inlines = [PadreTabularAdmin, MadreTabularAdmin, HermanoTabularAdmin, ParejaTabularAdmin]



class ConductorAdmin(admin.ModelAdmin):
    list_display = ['nombre_completo', 'licencia', 'entrada', 'salida', 'ultimo_viaje', 'estatus']
    
    search_fields = ['apellidos', 'licencia', 'estatus']

    list_filter = ['apellidos', 'genero', 'entrada']

    form = ConductorForm

    inlines = []


class PasajeroAdmin(admin.ModelAdmin):
    list_display = ['nombre_completo', 'viaje_origen', 'viaje_destino', 'viaje_salida', 'viaje_llegada', 'boleto', 'forma_pago', 'viaje_status']

    search_fields = ['apellidos', 'viaje_origen', 'viaje_destino', 'boleto', 'forma_pago']

    list_filter = ['apellidos', 'genero', 'forma_pago']

    form = PasajeroForm

    inlines = []


admin.site.register(Persona, PersonaAdmin)
admin.site.register(Pasajero, PasajeroAdmin)
admin.site.register(Conductor, ConductorAdmin)