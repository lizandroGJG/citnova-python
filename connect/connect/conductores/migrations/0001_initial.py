# Generated by Django 2.2.7 on 2019-11-16 21:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('genero', models.CharField(choices=[('M', 'MASCULINO'), ('F', 'FEMENINO')], max_length=1)),
                ('fecnac', models.DateField()),
                ('hermanos', models.ManyToManyField(blank=True, related_name='mis_hermanos', to='conductores.Persona')),
                ('madre', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hijos_de_la_madre', to='conductores.Persona')),
                ('padre', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hijos_del_padre', to='conductores.Persona')),
                ('pareja', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='mi_pareja', to='conductores.Persona')),
            ],
            options={
                'verbose_name': 'Personas',
                'verbose_name_plural': 'Personas',
            },
        ),
        migrations.CreateModel(
            name='Conductor',
            fields=[
                ('persona_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='conductores.Persona')),
                ('licencia', models.CharField(max_length=50)),
                ('entrada', models.TimeField()),
                ('salida', models.TimeField()),
            ],
            options={
                'verbose_name': 'Conductores',
                'verbose_name_plural': 'Conductores',
            },
            bases=('conductores.persona',),
        ),
        migrations.CreateModel(
            name='Pasajero',
            fields=[
                ('persona_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='conductores.Persona')),
                ('boleto', models.CharField(max_length=50)),
                ('forma_pago', models.IntegerField(choices=[(1, 'TARJETA'), (2, 'EFECTIVO')], default=2)),
            ],
            options={
                'verbose_name': 'Pasajeros',
                'verbose_name_plural': 'Pasajeros',
            },
            bases=('conductores.persona',),
        ),
    ]
