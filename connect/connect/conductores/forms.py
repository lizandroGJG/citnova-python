from django import forms
from .models import Conductor, Pasajero, Persona, Hermandad, GENERO_CHOICES, FORMAS_PAGO
from connect.viajes.models import Viaje
from django.forms import TextInput, Select

class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ['nombre', 'apellidos', 'genero', 'fecnac']
        widgets = {
            'nombre': TextInput(
                attrs = {
                    'class': 'form-control'
                }
            ),
            'apellidos': TextInput(
                attrs = {
                    'class': 'form-control'
                }
            ),
            'genero': Select(
                attrs = {
                    'class': 'form-control'
                }
            ),
            'fecnac': TextInput(
                attrs = {
                    'class': 'form-control'
                }
            ),
        }


class ConductorForm(forms.ModelForm):
    class Meta:
        model = Conductor
        fields = ['nombre', 'apellidos', 'genero', 'fecnac', 'licencia', 'entrada', 'salida']


class PasajeroForm(forms.ModelForm):
    class Meta:
        model = Pasajero
        fields = ['nombre', 'apellidos', 'genero', 'fecnac', 'viaje', 'boleto', 'forma_pago']


class HermandadFormSet(forms.BaseInlineFormSet):
    def get_from_kwargs(self, index):
        kwargs = super(HermandadFormSet, self).get_from_kwargs(index)
        kwargs.update({'parent': self.instance})
        return kwargs


class HermandadForm(forms.ModelForm):
    nombre    = forms.CharField(max_length=50, label='Nombre')
    apellidos = forms.CharField(max_length=50, label='Apellidos')
    genero    = forms.ChoiceField(choices=GENERO_CHOICES, label="Genero")
    fecnac    = forms.DateField(label="Fecha de Nacimiento")

    class Meta:
        models = Hermandad
        fields = ['persona_dos', 'nombre', 'apellidos', 'genero']

    def __init__(self, *args, **kwargs):
        self.parent = kwargs.get('parent', None)
        if self.parent is not None:
            del kwargs['parent']

        super(HermandadForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].required = False

            if self.parent is not None:
                self.fields['persona_dos'].queryset = Persona.objects.exclude(id=self.parent.id).all()
                self.fields['persona_dos'].label = 'Seleccionar Persona'

            if self.instance is not None and len(str(self.instance).strip())>0:
                self.fields['nombre'].initial = self.instance.persona_dos.nombre
                self.fields['apellidos'].initial = self.instance.persona_dos.apellidos
                self.fields['genero'].initial = self.instance.persona_dos.genero
                self.fields['fecnac'].initial = self.instance.persona_dos.fecnac
            else:
                pass