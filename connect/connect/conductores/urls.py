from django.urls import path, include
from .views import listado_personas, detalle_persona, registrar_persona, eliminar_persona

urlpatterns = [
    path('personas/', listado_personas, name='personas'),
    path('personas/<int:persona_id>', detalle_persona, name='detalle_persona'),
    path('personas/registrar/', registrar_persona, name='registrar_persona'),
    path('personas/eliminar/<int:persona_id>', eliminar_persona, name='eliminar_persona'),
]