from django.db import models
from datetime import date

COMBUSTIBLE_CHOICES = [
    ('1', 'DIESEL'),
    ('2', 'GASOLINA')
]

class Vehiculo(models.Model):
    placas = models.CharField(max_length=50, null=False, blank=False)
    vin = models.CharField(max_length=50, null=False, blank=False)
    marca = models.CharField(max_length=50, null=False, blank=False)
    modelo = models.CharField(max_length=50, null=False, blank=False)
    anio = models.DecimalField(max_digits=4, decimal_places=0, null=False, blank=False)
    veirifacion = models.CharField(max_length=50, null=False, blank=False)
    color = models.CharField(max_length=20, null=False, blank=False)
    combustible_tipo = models.CharField(max_length=1, null=False, blank=False, choices=COMBUSTIBLE_CHOICES, default='2')
    combustible_capacidad = models.IntegerField(null=False, blank=False, default=0)
    num_asientos = models.IntegerField(null=False, blank=False)
    num_puertas = models.IntegerField(null=False, blank=False)
    verificacion = models.DateField(default=date.today())

    class Meta:
        verbose_name = 'Vehiculos'
        verbose_name_plural = 'Vehiculos'

    def __str__(self):
        return '{}:{}:{}:{}'.format(self.marca, self.modelo, self.anio, self.placas)

    @property
    def vehiculo(self):
        return '{} {} {}'.format(self.marca, self.modelo, self.anio)

class Camioneta(Vehiculo):
    class Meta:
        verbose_name = 'Camionetas'
        verbose_name_plural = 'Camionetas'

class Autobus(Vehiculo):
    tiene_banio = models.BooleanField(null=False, default=True, blank=False)

    class Meta:
        verbose_name = 'Autobuses'
        verbose_name_plural = 'Autobuses'

    def __str__(self):
        return '{}: {} tiene baño'.format(self.placas, 'si' if self.tiene_banio else 'no')