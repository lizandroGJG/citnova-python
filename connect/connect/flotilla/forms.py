from django import forms
from django.forms import DateField

from .models import Vehiculo, Autobus, Camioneta


class VehiculoForm(forms.ModelForm):

    class Meta:
        model = Vehiculo
        fields = []

        labels = {
            'anio': 'Año',
        }

        help_texts = {
            'verificacion': 'Ultima fecha de verificación',
        }

        ANIOS_LIST = range(2000, 2019)
        ANIOS = [(2001, 2001), (2002, 2002)]
        # anio = forms.DateInput(widget=forms.SelectDateWidget(years=ANIOS_LIST))
        widgets = {
            # 'anio': forms.SelectDateWidget(years=ANIOS_LIST)
            # 'anio': forms.ChoiceField(widget=forms.Select, choices=[ANIOS_LIST], required=False)
            # 'anio': forms.Select(None, choices=[ANIOS])
            # 'anio': forms.DateField(widget=forms.SelectDateWidget(years=[ANIOS_LIST]))
        }

    def __init__(self, *args, **kwargs):
        super(VehiculoForm, self).__init__(*args, **kwargs)

        # self.fields['num_asientos'].required = False

        # for field in self.fields:
        #     if field in
