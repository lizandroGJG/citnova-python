from django.urls import path, include

from .views import (
    current_datetime,
    fetch_hidalgo,
    hola_bootstrap,
    index,
    get_vehiculos,
    add_vehiculo,
)

urlpatterns = [
    path('hoy/', current_datetime, name='hoy'),
    path('muni/', fetch_hidalgo, name='muni'),
    path('holabs/', hola_bootstrap, name='holabs'),


    path('', index, name='index'),
    path('get_vehiculos/', get_vehiculos, name='get_vehiculos'),
    path('add/', add_vehiculo, name='add_vehiculo'),
]
