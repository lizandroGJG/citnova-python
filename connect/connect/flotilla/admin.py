from django.contrib import admin
from datetime import date

from .models import Vehiculo, Autobus, Camioneta
from .forms import VehiculoForm


def verifica_vehiculo(modeladmin, request, queryset):
    for vehiculo in queryset:
        vehiculo.verificacion = date.today()
        vehiculo.save()


verifica_vehiculo.short_description = "Verificado Hoy"


# 'placas', 'vin', 'marca', 'modelo', 'anio', 'verifacion', 'color', 
# 'combustible_tipo', 'combustible_capacidad', 'num_asientos', 'num_puertas'
class VehiculoAdmin(admin.ModelAdmin):
    form = VehiculoForm
    list_display = ['vehiculo', 'color', 'placas', 'vin', 'num_puertas', 'num_asientos']
    search_fields = ['marca', 'modelo', 'anio', 'placas', 'vin', 'color']
    list_filter = ['marca', 'modelo', 'anio', 'color']

    fields = (('marca', 'modelo', 'anio'), ('placas', 'vin'), 'color', 'verificacion', ('num_asientos', 'num_puertas'))

    actions = [verifica_vehiculo]

    def vehiculo(self, obj):
        return obj.vehiculo


admin.site.register(Vehiculo, VehiculoAdmin)


class AutobusAdmin(admin.ModelAdmin):
    pass


class CamionetaAdmin(admin.ModelAdmin):
    pass


admin.site.register(Autobus, AutobusAdmin)
admin.site.register(Camioneta, CamionetaAdmin)