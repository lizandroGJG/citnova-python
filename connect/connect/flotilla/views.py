from django.http import HttpResponse
from django.shortcuts import render
import datetime
# import requests
from django.template import loader
from django.urls import reverse

from .models import Vehiculo
from .forms import VehiculoForm
from django.http import JsonResponse


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)

def fetch_hidalgo():
    pass


def hola_bootstrap(request):
    context = {}
    
    return render(request, 'flotilla/hola.html')


def index(request):
    # context = {}
    # return render(request, 'flotilla/index.html')
    personas = Vehiculo.objects.all()

    lista = []
    for index, persona in enumerate(personas):
        lista.append(
            {
                0: str(index + 1),
                1: persona.marca,
                2: persona.modelo,
                3: persona.placas,
                4: persona.color,
                'link': {
                    'url': 'flhnh',
                    'label': 'Detalle'
                },
            }
        )

    lista_data = {
        'lista': lista,
        'columnas': [
            'No.', 'Marca', 'Modelo', 'Placas', 'Color', 'Detalle'
        ]
    }

    context = {
        'lista': lista_data,
    }
    template = loader.get_template('flotilla/index.html')
    return HttpResponse(template.render(context, request))


def get_vehiculos(request):
    vehiculos = Vehiculo.objects.all().values()  # or simply .values() to get all fields
    vehiculos_list = list(vehiculos)  # important: convert the QuerySet to a list object
    return JsonResponse(vehiculos_list, safe=False)


def add_vehiculo(request):
    form = VehiculoForm()
    if request.method == 'POST':
        # form = VehiculoForm(request.POST)
        #cform = Vehiculo(request.POST)
        # form = Vehiculo(marca='asfafs', modelo='sdfdg', placas='djknsjdg')
        form = Vehiculo(marca=request.POST.get('marca',''), modelo=request.POST.get('modelo',''), placas=request.POST.get('placas',''))
        
        form.anio = 2019
        form.num_asientos = 15
        form.num_puertas = 3

        form.save()
        return JsonResponse([True], safe=False)

        '''
        form.marca = 'ksbdgs'
        form.modelo = 'ksbdgs'
        form.placas = 'ksbdgs'
        form.anio = 2019
        if form.is_valid():
            form.save()
            return JsonResponse([True], safe=False)
        '''